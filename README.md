The prototype gallery application
=================================

iOS SDK 8 without third-party libraries.

1. There is a list of images with a title and a small thumbnail.
2. "On-demand" loading.
3. Selecting any item in the list results is displaying a bigger image.
4. Ability to share any of the images by email.
5. Search an image by name.