
@import MessageUI;

#import "DetailViewController.h"
#import "InfoViewController.h"
#import "DataKeys.h"
#import "ImageLoader.h"
#import "ImageCache.h"
#import "AlertController.h"

@interface DetailViewController () <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *buttonShare;
@property (nonatomic, weak) IBOutlet UIButton *buttonInfo;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *aiLoading;

@property (nonatomic, assign) BOOL needsForScroll;
@property (nonatomic, assign) CGPoint scrollViewCenter;

@property (nonatomic, strong) ImageLoader *imageLoader;
@property (nonatomic, strong) ImageLoaderTask *task;

@end

@implementation DetailViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imageLoader = [[ImageLoader alloc] init];
}

- (void)setEntity:(NSDictionary *)entity {
    if (_entity != entity) {
        _entity = entity;
        [self loadImage];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
    self.navigationItem.leftItemsSupplementBackButton = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];
    [self adjustZoomOfScrollView];
    [self.scrollView addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew context:nil];
    [self.imageView addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.scrollView removeObserver:self forKeyPath:@"center"];
    [self.imageView removeObserver:self forKeyPath:@"bounds"];
}

#pragma mark - Actions & Segues 

#if TARGET_IPHONE_SIMULATOR
- (IBAction)buttonShareDidTouch {
    [AlertController showAlertWithTitle:@"No way dude!"
                                message:@"Please launch on the device."
                                   inVC:self];
    return;
}
#else
- (IBAction)buttonShareDidTouch {
    if (![MFMailComposeViewController canSendMail]) {
        [AlertController showAlertWithTitle:@"Error"
                                    message:@"You can't sand mail. It's need to configure Mail App."
                                       inVC:self];
        return;
    }
    
    NSString *emailTitle = @"Great photo from ProtoGallery";
    NSString *messageBody = @"Hey, check this out!";
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    
    NSString *filename = [self.entity objectForKey:DataKeyUrlImage];
    NSString *filePath = [defaultImageCache imagePathForKey:filename];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    [mc addAttachmentData:fileData mimeType:@"image/jpeg" fileName:@"photo.jpg"];
    
    [self presentViewController:mc animated:YES completion:nil];

}
#endif

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showInfo"]) {
        InfoViewController *vc = (InfoViewController *)[segue destinationViewController];
        [vc prepareData:[defaultImageCache imageInfoForKey:[self.entity objectForKey:DataKeyUrlImage]]];
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == self.scrollView) {
        if (!CGPointEqualToPoint(self.scrollView.center, self.scrollViewCenter)) {
            self.scrollViewCenter = self.scrollView.center;
            [self adjustZoomOfScrollView];
        }
    }
    if (object == self.imageView) {
        if (self.needsForScroll && self.imageView.image) {
            self.needsForScroll = NO;
            [self scrollImageToCenterAnimated:NO];
        }
    }
}

#pragma mark - Auxiliaries

- (void)loadImage {
    if (self.task) {
        self.task.completionBlock = nil;
    }
    NSString *url = [self.entity objectForKey:DataKeyUrlImage];
    if (url.length > 0) {
        __weak typeof(self) weakSelf = self;
        self.task = [self.imageLoader loadImageWithURL:[NSURL URLWithString:url] completion:
                     ^(UIImage *image, NSError *error) {
                         if (error) {
                             [weakSelf showError:error];
                         }
                         [weakSelf configureView];
                     }];
    }

}

- (void)showError:(NSError *)error {
    [AlertController showAlertWithTitle:@"Error"
                                message:error.description
                                   inVC:self.splitViewController];
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.entity) {
        self.navigationItem.title = [self.entity objectForKey:DataKeyName];
    }
    BOOL hasImage = !!(self.task.image);
    self.buttonShare.enabled = hasImage;
    self.buttonInfo.hidden = !hasImage;
    
    if (self.task) {
        if ((!self.task.error) && (!self.task.image)) {
            if (!self.aiLoading.isAnimating) [self.aiLoading startAnimating];
        } else {
            if (self.aiLoading.isAnimating) [self.aiLoading stopAnimating];
        }
    }
    if ((hasImage) && (self.task.image != self.imageView.image)) {
        self.imageView.image = self.task.image;
        [self adjustZoomOfScrollView];
        self.needsForScroll = YES;
    }
}

- (void)adjustZoomOfScrollView {
    CGSize scrollSize = self.scrollView.frame.size;
    CGSize imageSize = self.task.image.size;
    if (imageSize.width > 0 && imageSize.height > 0) {
        CGFloat minScale = MIN(scrollSize.width / imageSize.width, scrollSize.height / imageSize.height);
        CGFloat scale = MAX(scrollSize.width / imageSize.width, scrollSize.height / imageSize.height);
        if (minScale > 1.f) minScale = 1.f;
        if (scale > 1.f) scale = 1.f;
        self.scrollView.minimumZoomScale = minScale;
        self.scrollView.maximumZoomScale = 1.f;
        self.scrollView.zoomScale = scale;
    }
}

- (void)scrollImageToCenterAnimated:(BOOL)animated {
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    CGPoint offset = (CGPoint){
        (contentRect.size.width - self.scrollView.frame.size.width) * 0.5f,
        (contentRect.size.height - self.scrollView.frame.size.height) * 0.5f
    };
    [self.scrollView setContentOffset:offset animated:animated];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if (self.scrollView != scrollView) return nil;
    return self.contentView;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    //TODO: advanced handling
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
