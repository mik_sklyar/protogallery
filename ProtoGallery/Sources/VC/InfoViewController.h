
@import UIKit;

@interface InfoViewController : UITableViewController

- (void)prepareData:(NSDictionary *)data;

@end
