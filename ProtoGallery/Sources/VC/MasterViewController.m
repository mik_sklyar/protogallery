
#import "MasterViewController.h"
#import "DetailViewController.h"
#import "EntityTableCell.h"
#import "AlertController.h"
#import "ImageLoader.h"
#import "DataManager.h"
#import "DataKeys.h"
#import "Utils.h"

@implementation UISplitViewController (collapsing)

- (void)toggleMasterView {
    UIBarButtonItem *bb = self.displayModeButtonItem;
    [[UIApplication sharedApplication] sendAction:bb.action to:bb.target from:nil forEvent:nil];
}

@end

@interface MasterViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ImageLoader *imageLoader;

@property (nonatomic, copy) NSString *selectedID;

@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    
    self.imageLoader = [[ImageLoader alloc] init];
    [self loadData];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *reloadButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
     UIBarButtonSystemItemRefresh target:self action:@selector(loadData)];
    self.navigationItem.rightBarButtonItem = reloadButton;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:
     @selector(onDataChanged:) name:DataManagerDataChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:
     @selector(onDataError:) name:DataManagerDataLoadErrorNotification object:nil];
    
    [self.tableView reloadData];
    [self selectTableRow];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions & Segues

- (void)selectTableRow {
    NSUInteger idx = [dataManager.filteredData indexOfObjectPassingTest:
      ^BOOL(NSDictionary *entity, NSUInteger idx, BOOL *stop) {
          return ([self.selectedID isEqualToString:[entity objectForKey:DataKeyID]]);
      }];
    if (idx != NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
        [self.tableView selectRowAtIndexPath:indexPath animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)onDataChanged:(NSNotification *)notification {
    [self.tableView reloadData];
    [self selectTableRow];
}

- (void)onDataError:(NSNotification *)notification {
    
}

- (void)loadData {
    if (IsInternetReachable()) {
        [dataManager loadDataWithURL:[NSURL URLWithString:DataURL]];
    } else {
        [AlertController showAlertWithTitle:@"Error"
                                    message:@"Please check your internet connection"
                                       inVC:self.splitViewController];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *entity = [dataManager.filteredData objectAtIndex:indexPath.row];
        self.selectedID = [entity objectForKey:DataKeyID];

        //iOS7 backward compatibility
        DetailViewController *controller = segue.destinationViewController;
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            controller = (DetailViewController*)[segue.destinationViewController topViewController];
        }
        else if ([controller isKindOfClass:[UISplitViewController class]]) {
            controller = segue.destinationViewController;
        }
        controller.entity = entity;
        
        [self.splitViewController toggleMasterView];
    }
}

#pragma mark - UITextfieldDelegate

- (IBAction)textFieldDidChange:(UITextField *)textField {
    NSCharacterSet *const cset = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *filter = [textField.text stringByTrimmingCharactersInSet:cset];
    dataManager.strFilter = filter;
}

#pragma mark - UITableviewDelegate, UITableviewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [dataManager.filteredData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *const reuseCellID = @"EntityTableCell";
    EntityTableCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellID
                                                            forIndexPath:indexPath];
    NSDictionary *entity = [dataManager.filteredData objectAtIndex:indexPath.row];
    cell.textLabel.text = [entity objectForKey:DataKeyName];
    NSString *url = [entity objectForKey:DataKeyUrlThumb];
    if ([url length] > 0) {
        cell.imageTask = [self.imageLoader loadImageWithURL:[NSURL URLWithString:url]];
    }
    return cell;
}


// cell separator fix
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
