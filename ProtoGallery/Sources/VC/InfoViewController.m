
#import "InfoViewController.h"
#import "InfoTableCell.h"

static NSString *const infoCommonSectionKey = @"{Common}";

NSString *ImageIOLocalizedString(NSString *string) {
    //TODO: real localization if needed
    NSCharacterSet *const cs = [NSCharacterSet characterSetWithCharactersInString:@"{}"];
    string = [string stringByTrimmingCharactersInSet:cs];
    return string;
};


@interface InfoViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *sectionNames;

//contains data dictionaries of each sectionName
@property (strong, nonatomic) NSDictionary *sections;

//contains arrays of keys for objects of each section
@property (strong, nonatomic) NSArray *sectionsKeys;

@end

@implementation InfoViewController

#pragma mark - Data

- (void)prepareData:(NSDictionary *)data {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    
        NSMutableArray *sectionNames  = [NSMutableArray array];
        NSMutableDictionary *sections = [NSMutableDictionary dictionary];
        NSMutableDictionary *commonSection = [NSMutableDictionary dictionary];

        //normalize data if needed
        [data enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                obj = [self prepareDataSection:obj];
                [sectionNames addObject:key];
                [sections setObject:obj forKey:key];
                return;
            }
            
            if ([obj isKindOfClass:[NSArray class]]) {
                obj = [self prepareStringRepresentationForKey:key arrayValue:obj];
            }
            [commonSection setObject:obj forKey:key];
        }];
        
        //sort sections and put "common" section at the first place
        [sectionNames sortUsingComparator:^NSComparisonResult(NSString *str1, NSString *str2) {
            return [ImageIOLocalizedString(str1) compare:ImageIOLocalizedString(str2)];
        }];
        if ([commonSection count]) {
            [sectionNames insertObject:infoCommonSectionKey atIndex:0];
            [sections setObject:commonSection forKey:infoCommonSectionKey];
        }
        
        //sort row names in sections
        NSMutableArray *sortedSectionsKeys = [NSMutableArray array];
        [sectionNames enumerateObjectsUsingBlock:
         ^(NSString *sectionName, NSUInteger idx, BOOL *stop) {
             NSDictionary *section = [sections objectForKey:sectionName];
             NSArray *keys = [[section allKeys] sortedArrayUsingComparator:
             ^NSComparisonResult(NSString *str1, NSString *str2) {
                 return [ImageIOLocalizedString(str1) compare:ImageIOLocalizedString(str2)];
             }];
             [sortedSectionsKeys addObject:keys];
         }];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.sectionNames = [sectionNames copy];
            self.sections = [sections copy];
            self.sectionsKeys = [sortedSectionsKeys copy];
            [self reloadTableData];
        });
    });
}

- (NSDictionary *)prepareDataSection:(NSDictionary *)dict {

    NSMutableDictionary *result = [NSMutableDictionary dictionary];
                                   
    [dict enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
        if ([obj isKindOfClass:[NSArray class]]) {
            obj = [self prepareStringRepresentationForKey:key arrayValue:obj];
        }
        [result setObject:obj forKey:key];
    }];

    return [result copy];
}

- (NSString *)prepareStringRepresentationForKey:(NSString *)key
                                     arrayValue:(NSArray *)array {
    NSString *delimiter = @".";
    //TODO: change delimiter depending on the key if needed
    
    return [array componentsJoinedByString:delimiter];
}

- (void)reloadTableData {
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sections count];
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    return ImageIOLocalizedString([self.sectionNames objectAtIndex:section]);
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [[self.sectionsKeys objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *const reuseCellID = @"InfoTableCell";
    InfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellID];
    NSString *sectionName = [self.sectionNames objectAtIndex:indexPath.section];
    NSDictionary *section = [self.sections objectForKey:sectionName];
    NSString *key = [[self.sectionsKeys objectAtIndex:indexPath.section]
                     objectAtIndex:indexPath.row];
    NSString *value = [section objectForKey:key];
    
    [cell setTitle:ImageIOLocalizedString(key) value:value];
    
    return cell;
}

@end
