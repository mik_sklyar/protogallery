@import ObjectiveC;

#import "AlertController.h"

static NSString *alertControllerDefaultCloseText = @"Close";

#pragma mark - AlertControllerAction

@interface AlertControllerAction : NSObject

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) AlertControllerHandler handler;

+ (instancetype)actionWithTitle:(NSString *)title handler:(AlertControllerHandler) handler;

@end

@implementation AlertControllerAction

+ (instancetype)actionWithTitle:(NSString *)title handler:(AlertControllerHandler) handler
{
    AlertControllerAction *action = [[AlertControllerAction alloc] init];
    action.title = title;
    action.handler = handler;
    return action;
}

@end

#pragma mark - AlertController

@interface AlertController ()

@property (strong, nonatomic) NSMutableArray *actionItems;

@end

@implementation AlertController

- (NSMutableArray *)actionItems {
    if (!_actionItems) {
        _actionItems = [[NSMutableArray alloc] init];
    }
    return _actionItems;
}

#pragma mark - Public methods

+ (void)setDefaultCloseButtonTitle:(NSString *)title {
    alertControllerDefaultCloseText = [title copy];
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString*)text inVC:(UIViewController *)vc {
    
    AlertController *ac = [[AlertController alloc] init];
    ac.title = title;
    ac.message = text;
    [ac showInViewController:vc];
}

- (void)addButtonWithTitle:(NSString *)title handler:(AlertControllerHandler)handler {

    [self.actionItems addObject:[AlertControllerAction actionWithTitle:title handler:handler]];
}

- (void)showInViewController:(UIViewController *)viewContoller {
    
    void (^block)(void) = ^{
        [self showAlertControllerInViewController:viewContoller];
    };
    
    if ([self compatibilityNeeded]) {
        block = (self.style == AlertControllerStyleActionSheet) ?
        ^{
            [self showActionSheetInView:viewContoller.view];
        } :
        ^{
            [self showAlert];
        };
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:block];
}

#pragma mark - Auxiliaries

- (void)showAlertControllerInViewController:(UIViewController *)viewController {
    UIAlertController *alertController =
    [UIAlertController alertControllerWithTitle:self.title message:self.message preferredStyle:
     self.style == AlertControllerStyleActionSheet ? UIAlertControllerStyleActionSheet : UIAlertControllerStyleAlert];
    
    NSArray *items = [self.actionItems copy];
    
    for (NSUInteger i = 0; i < items.count; i++) {
        AlertControllerAction *item = [items objectAtIndex:i];
        UIAlertAction *alertAction =
        [UIAlertAction actionWithTitle:item.title style:
         ((item.handler) ? UIAlertActionStyleDefault : UIAlertActionStyleCancel) handler:
         ^(UIAlertAction *action) {
             NSInteger buttonIndex = i;
             if (item.handler) item.handler(buttonIndex);
         }];
        [alertController addAction:alertAction];
    }
    
    [alertController addAction:[UIAlertAction actionWithTitle:alertControllerDefaultCloseText
                                                        style:UIAlertActionStyleCancel handler:nil]];
    [viewController presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] init];
    alertView.title = self.title;
    alertView.message = self.message;
    alertView.delegate = self;

    NSArray *items = [self.actionItems copy];
    for (NSUInteger i = 0; i < items.count; i++) {
        [alertView addButtonWithTitle:[[items objectAtIndex:i] title]];
    }
    [alertView addButtonWithTitle:alertControllerDefaultCloseText];
    alertView.cancelButtonIndex = alertView.numberOfButtons - 1;
    
    [self associateWith:alertView];
    [alertView show];
}

- (void)showActionSheetInView:(UIView *)view {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    actionSheet.title = self.title;
    actionSheet.delegate = self;
    
    NSArray *items = [self.actionItems copy];
    for (NSUInteger i = 0; i < items.count; i++) {
        [actionSheet addButtonWithTitle:[[items objectAtIndex:i] title]];
    }
    [actionSheet addButtonWithTitle:alertControllerDefaultCloseText];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    [self associateWith:actionSheet];
    [actionSheet showInView:view.window];
}

- (BOOL)compatibilityNeeded {
    return [[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending;
}

- (NSString *)associationKey {
    static NSString *key;
    if (!key) {
        key = [NSString stringWithFormat:@"kei_%@", NSStringFromClass([self class])];
    }
    return key;
}

- (void)associateWith:(NSObject *)obj {
    objc_setAssociatedObject(obj, (__bridge const void *)([self associationKey]), self, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)dissociateWith:(NSObject *)obj {
    objc_setAssociatedObject(obj, (__bridge const void *)([self associationKey]), nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    AlertControllerAction *item = self.actionItems[buttonIndex];
    if (item.handler) {
        item.handler(buttonIndex);
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self dissociateWith:alertView];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    AlertControllerAction *item = self.actionItems[buttonIndex];
    if (item.handler) {
        item.handler(buttonIndex);
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self dissociateWith:actionSheet];
}

@end
