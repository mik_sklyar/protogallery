
#import "Utils.h"
#import "Reachability.h"

NSString *const NotificationErrorKey = @"NSError";

BOOL IsInternetReachable() {
    static dispatch_once_t onceToken;
    static Reachability *reachability = nil;
    dispatch_once(&onceToken, ^{
        reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
    });
    
    return ([reachability currentReachabilityStatus] != NotReachable);
}