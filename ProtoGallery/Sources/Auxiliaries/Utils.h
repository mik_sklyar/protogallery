
@import Foundation;

#define Error(__domain__, __code__) [NSError errorWithDomain:__domain__ code:__code__ userInfo:nil]

#define ErrorCover(__domain__, __code__, __error__) [NSError errorWithDomain:__domain__ code:__code__ userInfo:@{NSUnderlyingErrorKey: error}]

#define PostNotification(__name__, __object__) \
[[NSNotificationCenter defaultCenter] postNotificationName:__name__ object:__object__]

#define PostNotificationInfo(__name__, __object__, __info__) \
[[NSNotificationCenter defaultCenter] postNotificationName:__name__ object:__object__ userInfo:__info__]

FOUNDATION_EXPORT NSString *const NotificationErrorKey;

#define PostNotificationWithError(__name__, __object__, __error__) \
[[NSNotificationCenter defaultCenter] postNotificationName:__name__ object:__object__ userInfo:@{ NotificationErrorKey : __error__ }]

BOOL IsInternetReachable();