@import Foundation;
@import UIKit;

typedef NS_ENUM(NSInteger, AlertControllerStyle) {
    AlertControllerStyleAlert = 0,
    AlertControllerStyleActionSheet
};

typedef void(^AlertControllerHandler)(NSInteger buttonIndex);

@interface AlertController : NSObject <UIAlertViewDelegate, UIActionSheetDelegate>

@property (nonatomic) AlertControllerStyle style;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *message;

+ (void)setDefaultCloseButtonTitle:(NSString *)title;

+ (void)showAlertWithTitle:(NSString *)title message:(NSString*)text inVC:(UIViewController *)vc;

- (void)addButtonWithTitle:(NSString *)title handler:(AlertControllerHandler)handler;

- (void)showInViewController:(UIViewController *)viewContoller;

@end
