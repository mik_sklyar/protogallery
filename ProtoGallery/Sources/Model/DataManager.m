
#import "DataManager.h"
#import "DataKeys.h"
#import "Utils.h"

NSString *const DataManagerDataChangedNotification = @"DataManager_DC_N";
NSString *const DataManagerDataLoadErrorNotification = @"DataManager_DL_Err_N";

NSString *const DataManagerErrorDomain = @"DataManagerError";
const NSInteger DataManagerErrorIncorrectMIME = -1;
const NSInteger DataManagerErrorBrokenJson = -11;

#define DMLog(format, ...) NSLog(@"DataManager: " format, ## __VA_ARGS__)


@interface DataManager () <NSURLSessionTaskDelegate>

@property (strong, nonatomic) NSURLSession *session;
@property (strong, nonatomic) NSURLSessionTask *task;

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *filteredData;

@end

@implementation DataManager

#pragma mark - Lifecycle

+ (instancetype)manager {
    static dispatch_once_t onceToken;
    static DataManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] initOnce];
        [manager addObserver:manager forKeyPath:@"strFilter" options:NSKeyValueObservingOptionNew context:nil];
        [manager addObserver:manager forKeyPath:@"data" options:NSKeyValueObservingOptionNew context:nil];
    });
    return manager;
}

- (instancetype)initOnce {
    self = [super init];
    NSURLSessionConfiguration *conf =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:conf delegate:
                    nil delegateQueue:[NSOperationQueue mainQueue]];
    return self;
}

#pragma mark - Data

- (void)processData:(NSData *)data {
    NSError *error = nil;
    NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:
                          kNilOptions error:&error];
    if (error) {
        NSError *err = ErrorCover(DataManagerErrorDomain, DataManagerErrorBrokenJson, error);
        PostNotificationWithError(DataManagerDataLoadErrorNotification, self, err);
        return;
    }
    
    @try {
        NSMutableArray *numeredData = [NSMutableArray arrayWithCapacity:[dataArray count]];
        [dataArray enumerateObjectsUsingBlock:^(NSDictionary *entity, NSUInteger idx, BOOL *stop) {
            NSMutableDictionary *mEntity = [entity mutableCopy];
            [mEntity setObject:@(idx).description forKey:DataKeyID];
            [numeredData addObject:[mEntity copy]];
        }];
        self.data = numeredData;
    }
    @catch (NSException * e) {
        NSError *err = Error(DataManagerErrorDomain, DataManagerErrorBrokenJson);
        PostNotificationWithError(DataManagerDataLoadErrorNotification, self, err);
        return;
    }
    @finally {}
}

- (void)updateFilteredData {
   
    if ([self.strFilter length] > 0) {
        NSPredicate *predicate =
        [NSPredicate predicateWithBlock:^BOOL(NSDictionary *entity, NSDictionary *bindings)
         {
             return ([[entity objectForKey:DataKeyName] rangeOfString:
                      self.strFilter options:NSCaseInsensitiveSearch].length > 0);
         }];
        self.filteredData = [self.data filteredArrayUsingPredicate:predicate];
    } else {
        self.filteredData = self.data;
    }
    PostNotification(DataManagerDataChangedNotification, self);
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == self) {
        //keys: data & strFilter
        [self updateFilteredData];
    }
}

#pragma mark - Network

- (void)clearDownloadTask {
    [self.task cancel];
    self.task = nil;
}

- (void)loadDataWithURL:(NSURL *)url {
    if (self.task) {
        if ([self.task.originalRequest.URL isEqual:url]) {
            return;
        } else {
            [self clearDownloadTask];
        }
    }
    
    self.task =
    [self.session dataTaskWithURL:url completionHandler:
     ^(NSData *data, NSURLResponse *response, NSError *error) {
         if (!error) {
             DMLog(@"MIME type: %@", response.MIMEType);
//             if ([response.MIMEType rangeOfString:@"json" options:NSCaseInsensitiveSearch].length > 0) {
                 [self processData:data];
//             } else {
//                 DMLog(@"Incorrect MIME type of response: %@", response.MIMEType);
//                 NSError *err = Error(DataManagerErrorDomain, DataManagerErrorBrokenJson);
//                 PostNotificationWithError(DataManagerDataLoadErrorNotification, self, err);
//             }
         } else {
             if (error.code != NSURLErrorCancelled) {
                 DMLog(@"Request failed. Error:/n%@", error);
             }
             PostNotificationWithError(DataManagerDataLoadErrorNotification, self, error);
         }
         [self clearDownloadTask];
         
     }];
    [self.task resume];
}

@end
