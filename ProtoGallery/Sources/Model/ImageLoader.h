
@import UIKit;

typedef void (^ImageLoaderCompletionBlock)(UIImage *image, NSError *error);

@interface ImageLoaderTask : NSObject

@property (nonatomic, strong, readonly) UIImage *image;
@property (nonatomic, strong, readonly) NSError *error;
@property (nonatomic, assign, readonly) float progress;

@property (nonatomic, copy) ImageLoaderCompletionBlock completionBlock;

@end


@interface ImageLoader : NSObject

- (ImageLoaderTask *)loadImageWithURL:(NSURL *)url;

- (ImageLoaderTask *)loadImageWithURL:(NSURL *)url
                           completion:(ImageLoaderCompletionBlock)block;

- (void)cancel:(ImageLoaderTask *)task;

- (void)cancelAll;

@end