
@import Foundation;

//notifications
FOUNDATION_EXPORT NSString *const DataManagerDataChangedNotification;
FOUNDATION_EXPORT NSString *const DataManagerDataLoadErrorNotification;

//errors
FOUNDATION_EXPORT NSString *const DataManagerErrorDomain;
FOUNDATION_EXPORT const NSInteger DataManagerErrorIncorrectMIME;
FOUNDATION_EXPORT const NSInteger DataManagerErrorBrokenJson;


@interface DataManager : NSObject

@property (nonatomic, copy) NSString *strFilter;
@property (nonatomic, strong, readonly) NSArray *filteredData;

+ (instancetype)manager;
- (instancetype) init __attribute__((unavailable("no need to create excess instances, use singleton instead")));


//doesn't start new task if one is in progress with the same url, else kills previous
- (void)loadDataWithURL:(NSURL *)url;

@end

#define dataManager [DataManager manager]

