
@import ImageIO;

#import "ImageCache.h"

static NSString *const ImageCacheDefaultFolderName = @"DefaultImageCache";
static NSString *const ImageCacheInfoFilename = @"keys.plist";

#define fileManager [NSFileManager defaultManager]

@interface ImageCache ()

@property (nonatomic, copy, readonly) NSString *cachePath;
@property (nonatomic, strong, readonly) NSMutableDictionary *keys;

@end

@implementation ImageCache

#pragma mark - Lifecycle

+ (instancetype)defaultCache {
    static dispatch_once_t onceToken;
    static ImageCache *defaultCache = nil;
    dispatch_once(&onceToken, ^{
        defaultCache = [[ImageCache alloc] initWithFolderName:ImageCacheDefaultFolderName];
    });
    return defaultCache;
}

- (instancetype)initWithFolderName:(NSString *)folderName {
    if (!folderName.length) return nil;
    if (self = [super init]) {
        @try {
            NSString *path = [[self cachesPath] stringByAppendingPathComponent:folderName];
            if (![fileManager fileExistsAtPath:path]) {
                NSError *error = nil;
                [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES
                                        attributes:nil error:&error];
                if (error) return nil;
            }
            _cacheFolder = folderName;
            _cachePath = path;
        }
        @catch (NSException *exception) {
            return nil;
        }
        @finally {}
        
        NSString *infoFilename = [self.cachePath stringByAppendingPathComponent:ImageCacheInfoFilename];
        
        if ([fileManager fileExistsAtPath:infoFilename]) {
            NSData *plistData = [NSData dataWithContentsOfFile:infoFilename];
            NSError *error;
            NSPropertyListFormat format;
            _keys = [NSPropertyListSerialization propertyListWithData:plistData options:
                     NSPropertyListMutableContainersAndLeaves format:&format error:&error];
        }
        if (!_keys) {
            _keys = [NSMutableDictionary dictionary];
        }
    }
    return self;
}

- (NSString *)cachesPath {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)
            objectAtIndex:0];
}

- (void)saveKeys {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(saveKeys) object:nil];
    [self.keys writeToFile:[self.cachePath stringByAppendingPathComponent:ImageCacheInfoFilename]
                atomically:YES];
}

- (NSString *)imagePathForKey:(NSString *)key {
    NSString *path = [self.keys objectForKey:key];
    if (!path.length) return nil;
    path = [self.cachePath stringByAppendingPathComponent:path];
    return path;
}

- (NSDictionary *)imageInfoForKey:(NSString *)key {
    NSString *path = [self imagePathForKey:key];
    if (!path.length) return nil;
    
    NSURL *url = [NSURL fileURLWithPath:path];
    CGImageSourceRef source = CGImageSourceCreateWithURL((__bridge CFURLRef)url, nil);
    NSDictionary *info = CFBridgingRelease(CGImageSourceCopyPropertiesAtIndex(source, 0, nil));
    CFRelease(source);
    return info;
}

- (UIImage *)imageForKey:(NSString *)key {
    NSString *path = [self imagePathForKey:key];
    if (!path.length) return nil;
    
    //TODO: NSCache?
    return [UIImage imageWithContentsOfFile:path];
}

- (NSString *)copyImageToCache:(NSString *)tmpPath key:(NSString *)key {
    
    NSString *fileName = [self.keys objectForKey:key];
    if (!fileName.length) {
        fileName = [[NSUUID UUID] UUIDString];
    }
    NSString *filePath = [self.cachePath stringByAppendingPathComponent:fileName];
    
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:&error];
    }
    if (error) return nil;
    
    [fileManager copyItemAtPath:tmpPath toPath:filePath error:&error];
    if (error) return nil;
    
    [self.keys setObject:fileName forKey:key];
    [self performSelector:@selector(saveKeys) withObject:nil afterDelay:1.f];

    return filePath;
}

@end
