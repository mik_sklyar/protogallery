
@import UIKit;

@interface ImageCache : NSObject

@property (nonatomic, copy, readonly) NSString *cacheFolder;

+ (instancetype)defaultCache;
- (instancetype)init __attribute__((unavailable("please use 'initWithFolderName:' method instead")));
- (instancetype)initWithFolderName:(NSString *)folderName;

// returns filepath in the chache folder
- (NSString *)copyImageToCache:(NSString *)path key:(NSString *)key;

- (UIImage *)imageForKey:(NSString *)key;
- (NSDictionary *)imageInfoForKey:(NSString *)key;
- (NSString *)imagePathForKey:(NSString *)key;

@end

#define defaultImageCache [ImageCache defaultCache]
