
static NSString *const DataURL = @"http://www.xiag.ch/share/testtask/list.json";

static NSString *const DataKeyID = @"identifier";
static NSString *const DataKeyName = @"name";
static NSString *const DataKeyUrlImage = @"url";
static NSString *const DataKeyUrlThumb = @"url_tn";
