
#import "ImageLoader.h"
#import "ImageCache.h"
#import "Utils.h"
#import "Reachability.h"

@interface ImageLoaderTask ()
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, assign) float progress;
@property (nonatomic, copy) NSURL *url;
@property (nonatomic, copy) NSURLSessionDownloadTask *downloadTask;
@end

@implementation ImageLoaderTask
@end


@interface ImageLoader () <NSURLSessionDownloadDelegate>

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, assign) NSUInteger maxActiveTasks;
@property (nonatomic, strong) NSMutableDictionary *activeTasks;
@property (nonatomic, strong) NSMutableDictionary *plannedTasks;
@property (nonatomic, strong) NSMutableArray *taskQueue;
@property (nonatomic, strong) NSMutableDictionary *imageCache;

@end


@implementation ImageLoader

#pragma mark - Lifecycle

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self cancelAll];
}

- (instancetype)init {
    if (self = [super init]) {
        
        self.maxActiveTasks = 5;
        self.activeTasks = [NSMutableDictionary dictionary];
        self.plannedTasks = [NSMutableDictionary dictionary];
        self.taskQueue = [NSMutableArray array];
        
        NSURLSessionConfiguration *configuration =
        [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:configuration delegate:
                        self delegateQueue:[NSOperationQueue mainQueue]];
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(onInternetReachabilityNotification:)
         name:kReachabilityChangedNotification object:nil];
    }
    return self;
}

#pragma mark - Public

- (ImageLoaderTask *)loadImageWithURL:(NSURL *)url {
    return [self loadImageWithURL:url completion:nil];
}

- (ImageLoaderTask *)loadImageWithURL:(NSURL *)url completion:(ImageLoaderCompletionBlock)block {
    if (!url) return nil;
    //TODO: check url structure
    
    NSString *const strUrl = url.absoluteString;
    ImageLoaderTask *task = [self.activeTasks objectForKey:strUrl];
    if (!task) task = [self.plannedTasks objectForKey:strUrl];
    [self activateNextTaskIfNeeded];
    if (task) return task;
    
    
    task = [[ImageLoaderTask alloc] init];
    task.url = url;
    task.completionBlock = block;

    if ((task.image = [defaultImageCache imageForKey:strUrl])) {
        [self launchTaskCompletion:task];
        return task;
    }
    
    if ((!IsInternetReachable()) ||
        ((self.maxActiveTasks > 0) && ([self.activeTasks count] >= self.maxActiveTasks))) {
        [self.plannedTasks setObject:task forKey:strUrl];
        [self.taskQueue addObject:task];
    } else {
        [self activateTask:task];
    }
    return task;
}

- (void)cancel:(ImageLoaderTask *)task {
    if (!task) return;
    
    [task.downloadTask cancel];
    NSString *strUrl = task.url.absoluteString;
    [self.activeTasks removeObjectForKey:strUrl];
    [self.plannedTasks removeObjectForKey:strUrl];
    [self.taskQueue removeObject:task];
}

- (void)cancelAll {
    [self.activeTasks enumerateKeysAndObjectsUsingBlock:
     ^(id key, ImageLoaderTask *task, BOOL *stop) {
        [task.downloadTask cancel];
    }];
    [self.activeTasks removeAllObjects];
    [self.plannedTasks removeAllObjects];
    [self.taskQueue removeAllObjects];
}

#pragma mark - Auxiliaries

- (void)onInternetReachabilityNotification:(NSNotification *)notification {
    [self activateNextTaskIfNeeded];
}

- (void)activateNextTaskIfNeeded {
    if (!IsInternetReachable()) return;

    while (((self.maxActiveTasks == 0) || (self.maxActiveTasks > [self.activeTasks count]))
           && ([self.taskQueue count] > 0)) {
        ImageLoaderTask *nextTask = [self.taskQueue objectAtIndex:0];
        [self activateTask:nextTask];
    }
}

- (void)activateTask:(ImageLoaderTask *)task {
    NSString *const strUrl = task.url.absoluteString;
    
    [self.taskQueue removeObject:task];
    [self.plannedTasks removeObjectForKey:strUrl];
    [self.activeTasks setObject:task forKey:strUrl];
    task.downloadTask = [self.session downloadTaskWithURL:task.url];
    [task.downloadTask resume];
}

- (void)afterActiveTaskHasBeenCompleted:(ImageLoaderTask *)task {
    [self.activeTasks removeObjectForKey:task.url.absoluteString];
    [self activateNextTaskIfNeeded];
    [self launchTaskCompletion:task];
}

- (void)launchTaskCompletion:(ImageLoaderTask *)task {
    if (task.completionBlock) {
        dispatch_async(dispatch_get_main_queue(), ^{
            task.completionBlock(task.image, task.error);
        });
    }
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)dtask didCompleteWithError:(NSError *)error {
    
    ImageLoaderTask *task =
    [self.activeTasks objectForKey:dtask.originalRequest.URL.absoluteString];
    
    if ((error) && (error.code != NSURLErrorCancelled)) {
        task.error = error;
    }
    [self afterActiveTaskHasBeenCompleted:task];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)dtask
didFinishDownloadingToURL:(NSURL *)location {
    NSString *const strUrl = dtask.originalRequest.URL.absoluteString;
    ImageLoaderTask *task = [self.activeTasks objectForKey:strUrl];
    [defaultImageCache copyImageToCache:location.path key:strUrl];
    task.image = [defaultImageCache imageForKey:strUrl];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)dtask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    if (totalBytesExpectedToWrite <= 0) return;
    
    ImageLoaderTask *task =
    [self.activeTasks objectForKey:dtask.originalRequest.URL.absoluteString];
    task.progress = (Float64)totalBytesWritten/(Float64)totalBytesExpectedToWrite;
}


@end
