@import UIKit;

@interface InfoTableCell : UITableViewCell

- (void)setTitle:(NSString *)title value:(id)value;

@end
