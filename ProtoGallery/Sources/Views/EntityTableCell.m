
#import "EntityTableCell.h"
#import "ImageCache.h"

@interface EntityTableCell ()

@property (nonatomic, weak) IBOutlet UILabel *lTitle;
@property (nonatomic, weak) IBOutlet UIImageView *ivPreview;
@property (nonatomic, weak) IBOutlet UIImageView *viewBkgSelected;

@end

@implementation EntityTableCell

- (void)setImageTask:(ImageLoaderTask *)imageTask {
    if (_imageTask == imageTask) return;
    
    [_imageTask removeObserver:self forKeyPath:@"image"];
    _imageTask = imageTask;
    [_imageTask addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
    [self processTaskImage];
}

- (UILabel *)textLabel {
    return self.lTitle;
}

- (UIImageView *)imageView {
    return self.ivPreview;
}

#pragma mark - Lifecycle

- (void)dealloc {
    self.imageTask = nil;
}

- (void)awakeFromNib {
    UIView *v =  self.viewBkgSelected;
    [v removeFromSuperview];
    [self setSelectedBackgroundView:v];
}

- (void)prepareForReuse {
    self.imageView.image = [UIImage imageNamed:@"imgPlaceholder-normal"];
    self.imageView.highlightedImage = [UIImage imageNamed:@"imgPlaceholder-selected"];
    self.imageTask = nil;
}

#pragma mark - Appearance

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    self.lTitle.textColor = highlighted ? [UIColor whiteColor] : [UIColor blackColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.lTitle.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
}

- (void)processTaskImage {
    if (self.imageTask.image) {
        self.imageView.image = self.imageView.highlightedImage = self.imageTask.image;
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    [self processTaskImage];
}

@end
