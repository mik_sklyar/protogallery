
@import UIKit;

#import "ImageLoader.h"

@interface EntityTableCell : UITableViewCell

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, strong) ImageLoaderTask *imageTask;

@end
