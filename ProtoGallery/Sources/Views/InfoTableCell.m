
#import "InfoTableCell.h"

@interface InfoTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@end

@implementation InfoTableCell

- (void)setTitle:(NSString *)title value:(id)value
{
    self.labelTitle.text = [title stringByAppendingString:@":"];
    
    
    if ([value isKindOfClass:[NSNumber class]]) {
        value = [(NSNumber *)value stringValue];
    }
    if (![value isKindOfClass:[NSString class]]) {
        NSLog(@"Error! Incorrect object class: %@",NSStringFromClass([value class]));
        value = [value description];
    }
    value = [value stringByTrimmingCharactersInSet:
             [NSCharacterSet characterSetWithCharactersInString:@" "]];
    self.labelText.text = value;
}

@end
